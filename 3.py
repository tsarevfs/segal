import numpy as np
import sympy as sp
from sympy.solvers import nsolve
from scipy.optimize import newton_krylov, anderson
from math import *
import matplotlib.pyplot as plt
from common import *

def main():
	points = []
	T_axis = []
	V_points = []
	x_points = []
	T0 = 273
	nsteps = 10
	for xg_int in range(nsteps):
		xg = float(xg_int) / nsteps 
		T = 1100. + T0
		print "xg = ", xg
		reagents = ['AlCl3', 'GaCl', 'NH3', 'HCl', 'H2']

		#R7 = [(1, 'AlCl'), (1, 'NH3'), (-1, 'AlN'), (-1, 'HCl'), (-1, 'H2')]
		#R8 = [(2, 'AlCl2'), (2, 'NH3'), (-2, 'AlN'), (-4, 'HCl'), (-1, 'H2')]
		R9 = [(1, 'AlCl3'), (1, 'NH3'), (-1, 'AlN'), (-3, 'HCl')]
		R10 = [(1, 'GaCl'), (1, 'NH3'), (-1, 'GaN'), (-1, 'HCl'), (-1, 'H2')]
		#R11 = [(2, 'GaCl2'), (2, 'NH3'), (-2, 'GaN'), (-4, 'HCl'), (-1, 'H2')]
		#R11 = [(1, 'GaCl3'), (1, 'NH3'), (-1, 'GaN'), (-3, 'HCl')]


		k9 = equil_k(R9, T)
		k10 = equil_k(R10, T)

		#print "K1 = ", k1
		#print "K2 = ", k2
		#print "K3 = ", k3

		D = dict((i, diff_coeff(i, T)) for i in reagents)

		#print "D(T, i) = ", zip(D, reagents)

		G = dict(zip(reagents, [xg * 30., (1 - xg) * 30., 1500., 0.9 * 98470., 0.1 * 98470.]))

		e_AlCl3, e_GaCl, e_NH3, e_HCl, e_H2, x = sp.symbols('e_AlCl3 e_GaCl e_NH3 e_HCl e_H2 x')
		E = [e_AlCl3, e_GaCl, e_NH3, e_HCl, e_H2, x]

		f1 = e_AlCl3 * e_NH3 - k9 * x * (e_HCl ** 3)
		f2 = e_GaCl * e_NH3 - k10 * (1 - x) * e_HCl * e_H2
		f3 = D['HCl'] * (G['HCl'] - e_HCl) + 2 * D['H2'] * (G['H2'] - e_H2) + 3 * D['NH3'] * (G['NH3'] - e_NH3)
		f4 = 3 * D['AlCl3'] * (G['AlCl3'] - e_AlCl3) + D['GaCl'] * (G['GaCl'] - e_GaCl) + D['HCl'] * (G['HCl'] - e_HCl)
		f5 = D['AlCl3'] * (G['AlCl3'] - e_AlCl3) + D['GaCl'] * (G['GaCl'] - e_GaCl) - D['NH3'] * (G['NH3'] - e_NH3)
		f6 = D['AlCl3'] * (G['AlCl3'] - e_AlCl3) * (1 - x) - D['GaCl'] * (G['GaCl'] - e_GaCl) * x
		F = [f1, f2, f3, f4, f5, f6]

		W_symb = sp.Matrix(F).jacobian(E)

		FF = lambda x: calc_F(x, E, F)
		#E_vals = newton_krylov(FF, [10., 0.38, 1485.34, 97501.76, 989.83, 0.00074], f_tol=0.1)
		E_vals = anderson(FF, [5.978, 0.38, 1485.34, 97501.76, 989.83, 0.00074], f_tol=10)
		E_vals, n = Newton(np.array([10., 0.5, 1500., 100000., 1000., 0.9]), 0.001, E, F, W_symb)
		#E_vals = nsolve(F, E, [5.978, 0.38, 1485.34, 97501.76, 989.83, 0.00074], tol=30, maxsteps=1000)
		print [float(a) for a in E_vals]
		#print n
		
		R = 8314.0
		delta = 0.01

		G_flow = [D[reagents[i]] * (G[reagents[i]] - E_vals[i]) / R / T / delta for i in range(2)]

		V = v_algan(T)
		V_points.append(np.sign(V) * log(abs(V)))
		x_points.append(E_vals[5])
		points.append([np.sign(pt) * log(abs(pt)) for pt in G_flow])
		T_axis.append(xg)

	plot_points = zip(*points)
	plt.subplot(311)
	plt.plot(T_axis, plot_points[0], 'r',
		T_axis, plot_points[1], 'g',
		)
	plt.subplot(312)
	plt.plot(T_axis, x_points , 'r')
	plt.subplot(313)
	plt.plot(T_axis, V_points , 'r')
	plt.show()


if __name__ == '__main__':
	main()