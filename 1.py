import numpy as np
import sympy as sp
from sympy.solvers import nsolve
from math import *
import matplotlib.pyplot as plt
from common import *


def main():
	points = []
	T_axis = []
	V_points = []
	T0 = 273
	for T_int in range(350 + T0, 650 + T0, 20):
		T = float(T_int)
		print "T = ", T
		reagents = ['AlCl', 'AlCl2', 'AlCl3', 'HCl', 'H2']

		R1 = [(2, 'Al'), (2, 'HCl'), (-2, 'AlCl'), (-1, 'H2')]
		R2 = [(1, 'Al'), (2, 'HCl'), (-1, 'AlCl2'), (-1, 'H2')]
		R3 = [(2, 'Al'), (6, 'HCl'), (-2, 'AlCl3'), (-3, 'H2')]


		k1 = equil_k(R1, T)
		k2 = equil_k(R2, T)
		k3 = equil_k(R3, T)

		print "K1 = ", k1
		print "K2 = ", k2
		print "K3 = ", k3

		D = [diff_coeff(i, T) for i in reagents]

		print "D(T, i) = ", zip(D, reagents)

		G = 0., 0., 0., 10000., 0.

		e1, e2, e3, e4, e5 = sp.symbols('e1 e2 e3 e4 e5')
		E = [e1, e2, e3, e4, e5]

		f1 = e4 ** 2 - k1 * e1 ** 2 * e5
		f2 = e4 ** 2 - k2 * e2 * e5
		f3 = e4 ** 6 - k3 * e3 ** 2 * e5 ** 3
		f4 = D[3] * (G[3] - e4) + 2 * D[4] * (G[4] - e5)
		f5 = D[0] * (G[0] - e1) + 2 * D[1] * (G[1] - e2) + 3 * D[2] * (G[2] - e3) + D[3] * (G[3] - e4)
		F = [f1, f2, f3, f4, f5]

		W_symb = sp.Matrix(F).jacobian(E)


		#E_vals = nsolve(F, E, [0.1, 10., 1000., 0.01, 1000.], tol=0.001, maxsteps=1000, solver='mdnewton')
		E_vals, n = Newton([0.1, 10., 1000., 0.01, 1000.], 0.01, E, F, W_symb)
		print [float(a) for a in E_vals]
		print n
		
		R = 8314.0
		delta = 0.01

		G_flow = [D[i] * (G[i] - E_vals[i]) / R / T / delta for i in range(3)]

		points.append([np.sign(pt) * log(abs(pt)) for pt in G_flow])
		V = v_al(T)
		V_points.append(np.sign(V) * log(abs(V)))
		T_axis.append(T)

	plot_points = zip(*points)
	plt.subplot(211)
	plt.plot(T_axis, plot_points[0], 'r',
		T_axis, plot_points[1], 'g',
		T_axis, plot_points[2], 'b',
		)
	plt.subplot(212)
	plt.plot(T_axis, V_points , 'r')
	plt.show()


if __name__ == '__main__':
	main()