import numpy as np
import sympy as sp
from math import *
from data import read_data


dat = read_data()

def gibbs_energy(i, T):
	x = T / 10000.;
	FI_T = dat[i].fi[0] + dat[i].fi[1] * log(x) + dat[i].fi[2] / x ** 2  + dat[i].fi[3] / x + dat[i].fi[4] * x + dat[i].fi[5] * x ** 2 + dat[i].fi[6] * x ** 3
	return dat[i].H - FI_T * T

def diff_coeff(i, T):
	sigma_in = (dat[i].sigma + dat['N2'].sigma) / 2.
	epsil_in = (dat[i].epsil * dat['N2'].epsil) ** (0.5)
	mu_in = 2. * dat[i].mu * dat['N2'].mu / (dat[i].mu + dat['N2'].mu)
	omega = 1.074*(T / epsil_in) ** (-0.1604)
	P = 100000.

	return 2.628e-2 * T ** (1.5) / (P * sigma_in * omega * mu_in ** (0.5))

def delta_g(react, T):
	return sum([koef * gibbs_energy(reagent, T) for koef, reagent in react])

def equil_k(react, T):
	P = 100000.
	R = 8.314
	return exp(-delta_g(react, T) / R / T) / P

def calc_W(x, W_symb, E):
	subs_map = dict(zip(E, x))
	return sp.Matrix(W_symb).applyfunc(lambda expr : expr.evalf(subs=subs_map))

def calc_F(x, E, F):
	subs_map = dict(zip(E, x))
	return np.array([f.evalf(subs=subs_map) for f in F])

def Newton(x0, eps, E, F, W_symb):
	x_k = np.copy(x0)
	n = 0

	bad_comp = None

	while n < 200:
		A = calc_W(x_k, W_symb, E)
		B = -calc_F(x_k, E, F)
		dx = np.linalg.solve(A, B)
		bad_comp = max([abs(xi) for (i, xi) in enumerate(dx)]), n
		x_k += dx
		n += 1
		
		if abs(bad_comp[0]) < eps:
			break

	return x_k, bad_comp

def v_al(T):
	return (gibbs_energy('AlCl', T) + gibbs_energy('AlCl2', T) + gibbs_energy('AlCl3', T)) * (dat['Al'].mu / dat['Al'].ro) * 1.e9

def v_ga(T):
	return (gibbs_energy('GaCl', T) + gibbs_energy('GaCl2', T) + gibbs_energy('GaCl3', T)) * (dat['Ga'].mu / dat['Ga'].ro) * 1.e9

def v_algan(T):
	return (gibbs_energy('AlCl3', T) * (dat['AlN'].mu / dat['AlN'].ro) + gibbs_energy('GaCl', T) * (dat['GaN'].mu / dat['GaN'].ro)) * 1.e9
