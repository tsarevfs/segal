import re 

def num_or_none(s):
	try:
		return float(s)
	except ValueError, e:
		return None

class Data:
	def __init__(self, fields):
		self.phase = fields[0]
		self.T1 = num_or_none(fields[1])
		self.T2 = num_or_none(fields[2])
		self.H = num_or_none(fields[3])
		self.fi = [num_or_none(f) for f in fields[4:11]]
		self.mu = num_or_none(fields[11])
		self.sigma = num_or_none(fields[12])
		self.epsil = num_or_none(fields[13])
		self.ro = None
		

def read_data():
	fin = open('Bank_TD_Fragment.dat', 'r')

	data = {}

	#Ind Phase T1 T2 H f1 f2 f3 f4 f5 f6 f7 mu sigma epsil Comment 
	for line in fin.readlines():
		vals = re.sub(' +', ' ', line.strip()).split(' ')
		data[vals[0]] = Data(vals[1:])

	data['Al'].ro = 2690.
	data['Ga'].ro = 5900.
	data['AlN'].ro = 3200.
	data['GaN'].ro = 6150.

	return data

			

if __name__ == '__main__':
	d = read_data()
	print d['N2'].fi[6]
		